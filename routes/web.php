<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use App\Models\Product;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard')->middleware('check.carrito');


Route::get('lang/{lang}', function ($lang){
   session(['lang' => $lang]);
   App::setLocale($lang);
   return redirect()->back();
});
Route::get('products', [ProductController::class,'mostrar'])->name('products');

Route::get('add-to-cart/{id}', [ProductController::class,'addToCart']);
Route::get('/cart', [ProductController::class,'cart']);
Route::patch('update-cart',[ProductController::class,'update']);
Route::delete('remove-from-cart',[ProductController::class,'remove']);

Route::get('venta', [ProductController::class,'venta'])->middleware('venta');



