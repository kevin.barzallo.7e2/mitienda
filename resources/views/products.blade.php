
@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="card-header">
            <form method="get" action="/products" >
                <input name="price" type="text"/>
                <label>MAX</label>
                <input name="filter" type="radio" value="MAX">
                <label>MIN</label>
                <input name="filter" type="radio" value="MIN">
                <input id="btn" value="search" type="submit"/>
            </form>
            <br>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Products') }}</div>
                    <div class="card-body">
                        @foreach ($productos as $product)
                            <div class="contenedor">
                                <div class="product-img">
                                    <img src="data:image/jpeg;base64,{!! stream_get_contents($product->image) !!}"/>
                                </div>
                                <div class="product-info">
                                    <div class="product-text">
                                        <h1>{{$product->nombre}}</h1>
                                    </div>
                                    <div class="product-price-btn">
                                        <p><span>{{$product->precio}}</span>€</p>
                                        <p>Stock : <span>{{$product->stock}}</span></p>


                                        <a href="{{ url('add-to-cart/'.$product->id) }}" role="button"><button type="button">Añadir</button></a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        {{$productos->links() }}
    </div>

    <style>
        *{
            font-family: 'Raleway', sans-serif;
        }

        #btn{
            padding: 10px;
            background-color: black;
            color: white;
            cursor: pointer;
        }
        .contenedor {
            height: 420px;
            width: 50%;
            margin: 50px auto;
            border-radius: 10px;
            display: flex;
            background-color: #ffffff;

        }

        .product-img {
            width: 50%;
        }

        .product-img img {
            border-radius: 10px;
            max-width: 100%;
            max-height: 100%;
        }

        .product-info {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            width: 50%;
            border-radius: 0 7px 10px 7px;
            background-color: #ffffff;
        }

        .product-text h1 {
            font-size: 34px;
        }

        .product-price-btn button {
            display: inline-block;
            height: 50px;
            width: 176px;
            border-radius: 40px;
            font-weight: 500;
            text-transform: uppercase;
            letter-spacing: 0.2em;
            color: #ffffff;
            background-color: #990033;
        }

    </style>

@endsection




