<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function cart_items(){
        $this->belongsToMany("App\Models\Cart_Items");

    }
}
