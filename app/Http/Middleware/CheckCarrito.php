<?php

namespace App\Http\Middleware;

use App\Models\Cart;
use App\Models\Product;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CheckCarrito
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user()->id;
        $cart_db = Cart::where("user_id",$user)->first();

        $cart_items = DB::select(DB::raw("select * from cart_items where cart_id = $cart_db->id"));

        foreach ($cart_items as $item){
            $id = $item->product_id;
            $product = Product::find($id);
            $cart[$id] = [
                "nombre" => $product->nombre,
                "cantidad" => $item->amount,
                "precio" => $product->precio,
                "image" => $product->image
            ];
            session()->put('cart', $cart);
        }
        return $next($request);
    }
}
