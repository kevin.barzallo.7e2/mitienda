<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('products')->insert([
            'nombre' => 'Tux de oro',
            'precio' => 500,
            'stock' => 5,
            'descripcion' => 'description product',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/tux.jpg'))
        ]);

        DB::table('products')->insert([
            'nombre' => 'Tux de plata',
            'precio' => 200,
            'stock' => 5,
            'descripcion' => 'description product',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/tux.jpg'))
        ]);

        DB::table('products')->insert([
            'nombre' => 'Tux de bronce',
            'precio' => 100,
            'stock' => 5,
            'descripcion' => 'description product',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/tux.jpg'))
        ]);

    }
}
